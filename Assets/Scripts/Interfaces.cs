﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICameraView
{
    void ChangeCamera(StateName new_state);
}

public interface IAudioSource
{
    void PlaySound();
    void PauseSound();
    void StopSound();
}

public interface IMoveThingController
{
    void Init();
    AbstractMove GetMove();

    void PlayAudio();
    void ChangeCamera(StateName new_state);
}

public interface IStaticController
{

}

public interface IGameController
{
    void StartGame();
    void EndGame();

    void PlayAudioMachine();
    void ChangeMachineCamera(StateName new_state);
}

public interface IViewModel
{
    void InitView(object[] parts);
    void ChangeState(StateName new_state);
}

public interface IViewUI
{
    void ChangeViewEndGame();
    void UpdateScores(int scores);
    bl_Joystick GetJoystickCamera();
    bl_Joystick GetJoystickMachine();
}

public abstract class AbstractMove : MonoBehaviour
{
    [SerializeField]
    protected float speed = 1f;

    private void FixedUpdate()
    {
        NextStep();
    }

    protected abstract void NextStep();
    public abstract float GetSpeed();
}

public interface IObserver
{
    void ReceiveMessage(MessageType type, object[] message);
    void RegisterInMessager();
    void RemoveFromMessager();
}
