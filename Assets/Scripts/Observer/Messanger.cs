﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Messanger : MonoBehaviour
{
    private static Dictionary<MessageType, List<IObserver>> observers;

    void Awake()
    {
        observers = new Dictionary<MessageType, List<IObserver>>();
    }

    public static void RegisterObserver(IObserver observer, MessageType type)
    {
        if(!observers.ContainsKey(type))
        {
            observers.Add(type, new List<IObserver>());
        }
        observers[type].Add(observer);
    }

    public static void RemoveObserver(IObserver observer)
    {
        foreach(var obs in observers)
        {
            if(obs.Value.Find(x => x == observer) != null)
            {
                obs.Value.Remove(observer);
            }
        }
    }

    public static void SendMessage(MessageType type, object[] message)
    {
        if(observers.Count != 0)
        {
            foreach (var obs in observers[type])
            {
                obs.ReceiveMessage(type, message);
            }
        }            
    }

}

public enum MessageType { station_trigger_enter, station_trigger_exit, stations_complete, load_cargo, stop_time }