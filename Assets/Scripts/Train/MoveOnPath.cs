﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeneratePath;

namespace Train
{
    public class MoveOnPath : AbstractMove
    {
        private Vector3[] path;
        private float forwardTime = 0f;
        private float step = 0f;
        private bool change_rotation = false;
        private bool move_forward = true;
        private bool is_move = false;
        private int index = 0;
        private bl_Joystick joystick;

        private void Start()
        {
            path = FindObjectOfType<PathPoints>().PathCoords();
            joystick = FindObjectOfType<PlayerInterface>().GetJoystickMachine();

            transform.position = path[index];
            transform.rotation = Quaternion.LookRotation(
                Vector3.RotateTowards(transform.position, path[index + 1] - transform.position, Mathf.PI, 0f)
                );          
        }

        protected override void NextStep()
        {
            transform.rotation = Quaternion.Slerp(
                transform.rotation, Quaternion.LookRotation(NextTarget() - transform.position), Time.deltaTime * 3f
                );

            if (Input.GetKey(KeyCode.W) || joystick.Vertical > 0.5f)
            {
                is_move = true;
                transform.position = Vector3.Lerp(path[index], NextTarget(), forwardTime * speed);
                forwardTime += Time.deltaTime;

                if (Vector3.Distance(transform.position, path[index + 1]) < 0.001f)
                {
                    ChangeIndex();
                    forwardTime = 0f;
                }
            }
            else
            {
                is_move = false;
            }
        }

        public override float GetSpeed()
        {
            return is_move ? 1 : 0;
        }

        private Vector3 NextTarget()
        {
            if(move_forward)
            {
                return path[index + 1];
            }
            else
            {
                return path[index - 1];
            }
        }

        private void ChangeIndex()
        {
            if(move_forward)
            {
                index++;
            }
            else
            {
                index--;
            }
        }
    }
}
