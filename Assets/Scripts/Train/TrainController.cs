﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Train
{
    public class TrainController : MonoBehaviour, IMoveThingController
    {
        public string info_name = "TrainInfo";
        
        private IInfo train_info;
        private IViewModel train_view;

        private AbstractMove move_controller;
        private IAudioSource audio_train;
        private ICameraView train_camera;

        private void Start()
        {
            train_info = (IInfo)Resources.Load(info_name);
            train_view = GetComponentInChildren<IViewModel>();
            audio_train = GetComponentInChildren<IAudioSource>();
            train_camera = GetComponentInChildren<ICameraView>();
            move_controller = GetComponentInChildren<AbstractMove>();
            Init();
        }

        public void Init()
        {
            train_view.InitView(train_info.GetView());
        }

        public AbstractMove GetMove()
        {
            return move_controller;
        }

        public void PlayAudio()
        {
            audio_train.PlaySound();
        }

        public void ChangeCamera(StateName new_state)
        {
            train_camera.ChangeCamera(new_state);
        }
    }
}
