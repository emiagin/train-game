﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour, ICameraView
{
    public Transform cam_fixed_pos;
    public Transform cam_flex_parent;
    public float speed = 5f;

    private Camera camera;
    private StateMachine state_machine;
    private bool is_move_cam = false;
    private bl_Joystick joystick;

    void Start()
    {
        camera = GetComponentInChildren<Camera>();
        joystick = FindObjectOfType<PlayerInterface>().GetJoystickCamera();

        state_machine = new StateMachine(new State[2] {
                new State(StateName.CameraFixed, SetCameraFixed),
                new State(StateName.CameraFlex, SetCameraFlex)
            }, StateName.CameraFixed);
    }

    void FixedUpdate()
    {
        if(is_move_cam)
        {
            MoveCam();
        }
    }

    private void MoveCam()
    {
        float v = joystick.Vertical; 
        float h = joystick.Horizontal;

        camera.transform.Rotate(-v * speed * Time.deltaTime, 0, 0);//, Space.World);
        camera.transform.Rotate(0, h * speed * Time.deltaTime, 0, Space.World);

       // camera.transform.Rotate((new Vector3(-v, h, 0) * Time.deltaTime) * speed);
    }

    public void ChangeCamera(StateName new_state)
    {
        state_machine.SetState(new_state);
    }

    private void SetCameraFlex()
    {
        camera.transform.parent = cam_flex_parent;
        camera.transform.localPosition = Vector3.zero;
        camera.transform.localRotation = Quaternion.identity;

        is_move_cam = true;
    }

    private void SetCameraFixed()
    {
        camera.transform.parent = cam_fixed_pos;
        camera.transform.localPosition = Vector3.zero;
        camera.transform.localRotation = Quaternion.identity;

        is_move_cam = false;
    }
}
