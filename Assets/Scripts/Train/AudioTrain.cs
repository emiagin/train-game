﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrain : MonoBehaviour, IAudioSource
{
    public AudioClip beep;
    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = beep;
        StopSound();
    }

    public void PauseSound()
    {
        source.Pause();
    }

    public void PlaySound()
    {
        source.PlayOneShot(beep);
    }

    public void StopSound()
    {
        source.Stop();
    }
}
