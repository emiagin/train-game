﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Train
{
    public class TrainView : MonoBehaviour, IViewModel
    {
        public GameObject body;

        private MeshFilter body_meshfilter;
        private MeshRenderer body_meshrenderer;

        private void Awake()
        {
            body_meshfilter = body.GetComponent<MeshFilter>();
            body_meshrenderer = body.GetComponent<MeshRenderer>();
        }

        public void InitView(object[] parts)
        {
            foreach(var part in parts)
            {
                if(part is Material)
                {
                    body_meshrenderer.material = (Material)part;
                }
                if(part is Mesh)
                {
                    body_meshfilter.mesh = (Mesh)part;
                }
            }
        }

        public void ChangeState(StateName new_state)
        {
            throw new System.NotImplementedException();
        }
    }
}
