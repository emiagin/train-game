﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabController : MonoBehaviour
{
    public GameObject[] prefabs;
    public NamePrefab[] prefab_names;

    static private Dictionary<NamePrefab, GameObject> prefabs_dic;

    private void Start()
    {
        prefabs_dic = new Dictionary<NamePrefab, GameObject>();

        for(int i = 0; i < prefabs.Length; i++)
        {
            prefabs_dic.Add(prefab_names[i], prefabs[i]);
        }
    }

    static public GameObject CreatePrefab(NamePrefab name, Transform parent = null)
    {        
        return Instantiate(prefabs_dic[name], parent);
    }

    static public GameObject CreateEmpty(Transform parent = null)
    {
        GameObject temp = Instantiate(new GameObject(), parent);
        temp.transform.localPosition = Vector3.zero;
        temp.transform.localRotation = Quaternion.identity;
        return temp;
    }
}

public enum NamePrefab { station, train }