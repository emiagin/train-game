﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GeneratePath
{
    public class PathPoints : MonoBehaviour
    {
        private Vector3[] vec_points;
        private Transform[] vec_station_places;
        private Point[] points;

        private void Awake()
        {
            points = GetComponentsInChildren<Point>().ToArray();
            vec_points = points.Select(x => x.transform.position).ToArray();
            vec_station_places = GetComponentsInChildren<StationPlace>().Select(x => x.transform).ToArray();
        }

        public Vector3[] PathCoords()
        {
            return vec_points;
        }

        public Transform[] StationPlaces()
        {
            return vec_station_places;
        }

        public Point[] GetPoints()
        {
            return points;
        }
    }
}
