﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneratePath
{
    public class PathLine : MonoBehaviour
    {
        private PathPoints generator_points;
        private LineRenderer path_line;

        private void Start()
        {
            generator_points = FindObjectOfType<PathPoints>();
            path_line = GetComponentInChildren<LineRenderer>();
            GenerateLine();
        }

        private void GenerateLine()
        {
            path_line.positionCount = generator_points.PathCoords().Length;
            path_line.SetPositions(generator_points.PathCoords());
        }
    }
}
