﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneratePath
{
    public class StationsCreator : MonoBehaviour
    {
        public string info_name = "StationInfo";        

        private IInfo station_info;
        private PathPoints generator_points;

        private List<Transform> station_places;        

        private void Start()
        {
            generator_points = FindObjectOfType<PathPoints>();
            station_places = new List<Transform>();
            station_info = (IInfo)Resources.Load(info_name);

            Create(generator_points.GetPoints());
        }

        private void Create(Point[] points)
        {            
            foreach(var point in points)
            {
                if(point.have_station)
                {
                    station_places.Add(PrefabController.CreatePrefab(NamePrefab.station, transform).transform);
                    station_places[station_places.Count - 1].position = point.GetComponentInChildren<StationPlace>().transform.position;
                    station_places[station_places.Count - 1].rotation = point.GetComponentInChildren<StationPlace>().transform.rotation;                    
                }                
            }
            Messanger.SendMessage(MessageType.stations_complete, null);
        }
    }
}
