﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TrainInfo : ScriptableObject, IInfo
{
    public Mesh mesh;
    public Material material;

    public object[] GetView()
    {
        return new object[2] { mesh, material };
    }
}
