﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInfo
{
    object[] GetView();
}

[CreateAssetMenu]
public class StationInfo : ScriptableObject, IInfo
{
    public Mesh mesh;
    public Material material;
    public int box_counts;

    public object[] GetView()
    {
        return new object[2] { mesh, material };
    }
}
