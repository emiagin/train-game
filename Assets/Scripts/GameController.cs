﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Train;

public class GameController : MonoBehaviour, IGameController, IObserver
{
    private IViewUI player_interface;
    private IStaticController[] stations;
    private IMoveThingController train;

    private IEnumerator coroutine_waittrain;

    private int _scores;
    protected int scores
    {
        get { return _scores; }
        set
        {
            _scores = value;
            player_interface.UpdateScores(_scores);
        }
    }

    void Awake()
    {
        train = GameObject.FindGameObjectWithTag("train").GetComponent<IMoveThingController>();
        player_interface = GameObject.FindGameObjectWithTag("player_interface").GetComponent<IViewUI>();
        RegisterInMessager();
        StartGame();
    }

#region Gameplay
    public void StartGame()
    {
        Time.timeScale = 1f;
        AddScores(0);
    }

    public void EndGame()
    {
        Time.timeScale = 0f;
        player_interface.ChangeViewEndGame();
    }

    private void AddScores(int number)
    {
        scores = number;
    }

    private IEnumerator WaitForStopTrain(IStaticController station)
    {
       // Debug.Log("enter waitforstoptrain");
        while(true)
        {
            //Debug.Log(train.GetMove().GetSpeed());
            if (train.GetMove().GetSpeed() == 0)
            {
                Messanger.SendMessage(MessageType.load_cargo, new object[1] { station });
                AddScores(scores + 1);                
                StopCoroutine(coroutine_waittrain);
            }
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion
#region ControllObjects
    public void PlayAudioMachine()
    {
        train.PlayAudio();
    }

    public void ChangeMachineCamera(StateName new_state)
    {
        train.ChangeCamera(new_state);
    }
#endregion
#region Init
    private void InitStations()
    {
        stations = GameObject.FindGameObjectsWithTag("station").Select(x => x.GetComponent<IStaticController>()).ToArray();
    }
#endregion
#region Observer
    public void ReceiveMessage(MessageType type, object[] message)
    {
        switch(type)
        {
            case MessageType.station_trigger_enter:
               // Debug.Log("receive message station_trigger_enter");
                coroutine_waittrain = WaitForStopTrain((IStaticController)message[0]);
                StartCoroutine(coroutine_waittrain);
                break;
            case MessageType.station_trigger_exit:
                StopCoroutine(coroutine_waittrain);
                break;
            case MessageType.stations_complete:
                InitStations();
                break;
            case MessageType.stop_time:
                StopAllCoroutines();
                EndGame();
                break;
        }
    }

    public void RegisterInMessager()
    {
        Messanger.RegisterObserver(this, MessageType.station_trigger_enter);
        Messanger.RegisterObserver(this, MessageType.station_trigger_exit);
        Messanger.RegisterObserver(this, MessageType.stations_complete);
        Messanger.RegisterObserver(this, MessageType.stop_time);
    }

    public void RemoveFromMessager()
    {
        Messanger.RemoveObserver(this);
    }
#endregion
}
