﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour, IViewUI
{
    public GameObject end_wnd;
    public Button btn_camera_flex;
    public Button btn_camera_fixed;
    public Button btn_trainaudio;
    public Button btn_restart;
    public Text[] score_txt;
    public Timer timer;
    public bl_Joystick joy_train;
    public bl_Joystick joy_camera;

    private IGameController game_controller;

    void Start()
    {
        game_controller = GameObject.FindGameObjectWithTag("game_controller").GetComponent<IGameController>();
        InitView(null);        
    }

    public void ChangeViewEndGame()
    {
        end_wnd.SetActive(true);
    }

    public void InitView(object[] parts)
    {
        btn_trainaudio.onClick.AddListener(TrainAudioBtn);
        btn_camera_flex.onClick.AddListener(CameraFlexBtn);
        btn_camera_fixed.onClick.AddListener(CameraFixedBtn);
        btn_restart.onClick.AddListener(RestartBtn);

        end_wnd.SetActive(false);
        joy_camera.gameObject.SetActive(false);
        timer.StartTimer();
    }

    public void RestartBtn()
    {
        SceneManager.LoadScene("main");
    }

    public void CameraFlexBtn()
    {
        game_controller.ChangeMachineCamera(StateName.CameraFlex);
        joy_camera.gameObject.SetActive(true);
    }

    public void CameraFixedBtn()
    {
        game_controller.ChangeMachineCamera(StateName.CameraFixed);
        joy_camera.gameObject.SetActive(false);
    }

    public void TrainAudioBtn()
    {
        game_controller.PlayAudioMachine();
    }

    public void UpdateScores(int scores)
    {
        foreach(var txt in score_txt)
        {
            txt.text = scores.ToString();
        }
    }

    public bl_Joystick GetJoystickCamera()
    {
        return joy_camera;
    }

    public bl_Joystick GetJoystickMachine()
    {
        return joy_train;
    }
}
