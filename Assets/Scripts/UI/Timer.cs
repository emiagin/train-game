﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float start_time;
    private float current_time;
    [SerializeField]
    private Text timer_txt;

    public void StartTimer(float _start_time = 300f)
    {
        start_time = _start_time;
        current_time = start_time;
        StartCoroutine("GoTimer");
    }

    private IEnumerator GoTimer()
    {
        while(true)
        {
            if (current_time > 0)
            {
                current_time -= Time.deltaTime;
            }
            else
            {
                current_time = 0f;
                Messanger.SendMessage(MessageType.stop_time, null);
                StopCoroutine("GoTimer");
            }
            UpdTimer();
            yield return new WaitForEndOfFrame();
        }        
    }

    private void UpdTimer()
    {
        timer_txt.text = ((int)(current_time / 60f)).ToString("00") + ":" + (current_time % 60f).ToString("00");
    }
}
