﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    public State[] states;

    private State _current_state;
    public State current_state
    {
        get { return _current_state; }
        private set
        {                     
            if(_current_state != null && _current_state.onEndState != null)
                _current_state.onEndState();
            _current_state = value;
            if (_current_state.onStartState != null)
                _current_state.onStartState();

        }
    }

    public StateMachine(State[] states, StateName current_state)
    {
        this.states = states;
        SetState(current_state);
    }

    public void SetState(StateName new_state)
    {
        foreach(var state in states)
        {
            if(state.name == new_state)
            {
                current_state = state;
            }
        }        
    }
}

public class State
{
    public StateName name;
    public Action onStartState, onEndState;

    public State(StateName name, Action onStartState = null, Action onEndState = null)
    {
        this.name = name;
        this.onStartState = onStartState;
        this.onEndState = onEndState;
    }        
}

public enum StateName { TrainArrived, TrainLeave,
                        StationFull, StationEmpty,
                        CameraFixed, CameraFlex }
