﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Station
{
    public class StationController : MonoBehaviour, IStaticController, IObserver
    {
        public string info_name = "StationInfo";
        private IInfo station_info;
        private IViewModel station_view;
        private StationTrigger trigger;

        private StateMachine state_machine;
        public bool end_path;

        private void Start()
        {
            RegisterInMessager();

            station_info = (IInfo)Resources.Load(info_name);
            station_view = GetComponent<IViewModel>();
            trigger = GetComponentInChildren<StationTrigger>();
            Init();

            state_machine = new StateMachine(new State[2] {
                new State(StateName.TrainArrived),
                new State(StateName.TrainLeave)
            }, StateName.TrainLeave);
        }

        public void Init()
        {
            station_view.InitView(station_info.GetView());
            trigger.main_station = this;
        }

    #region Observer
        public void ReceiveMessage(MessageType type, object[] message)
        {
            switch (type)
            {
                case MessageType.load_cargo:
                    if((IStaticController)message[0] == this)
                    {
                        station_view.ChangeState(StateName.StationEmpty);
                    }
                    break;
            }
        }

        public void RegisterInMessager()
        {
            Messanger.RegisterObserver(this, MessageType.load_cargo);
        }

        public void RemoveFromMessager()
        {
            Messanger.RemoveObserver(this);
        }
    #endregion
    }
}
