﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Station
{
    public class StationView : MonoBehaviour, IViewModel
    {
        public GameObject body;
        public ParticleSystem particles;

        private MeshFilter body_meshfilter;
        private MeshRenderer body_meshrenderer;

        private StateMachine state_machine;

        private void Awake()
        {
            body_meshfilter = body.GetComponent<MeshFilter>();
            body_meshrenderer = body.GetComponent<MeshRenderer>();

            state_machine = new StateMachine(new State[2] {
                new State(StateName.StationEmpty, RemoveBoxes),
                new State(StateName.StationFull, SetBoxes)
            }, StateName.StationFull);
        }

        public void InitView(object[] parts)
        {
            foreach (var part in parts)
            {
                if (part is Material)
                {
                    body_meshrenderer.material = (Material)part;
                }
                if (part is Mesh)
                {
                    body_meshfilter.mesh = (Mesh)part;
                }
            }
        }

        public void ChangeState(StateName new_state)
        {
            state_machine.SetState(new_state);
        }

        private void SetBoxes()
        {

        }

        private void RemoveBoxes()
        {

        }        
    }
}
