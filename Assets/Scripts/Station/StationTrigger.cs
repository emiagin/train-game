﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Station
{
    public class StationTrigger : MonoBehaviour
    {
        [HideInInspector]
        public IStaticController main_station;        

        public void OnTriggerEnter(Collider other)
        {
            if (other.tag == "train_collider")
            {
                //Debug.Log("enter");
                Messanger.SendMessage(MessageType.station_trigger_enter, new object[1] { main_station });
            }
        }

        public void OnTriggerExit(Collider other)
        {
            if (other.tag == "train_collider")
            {
               // Debug.Log("exit");
                Messanger.SendMessage(MessageType.station_trigger_exit, new object[1] { main_station });
            }
        }
    }
}